# split in half
# m = n / 2

# # recursive sorts
# sort a[1..m]
# sort a[m+1..n]

# merge sorted sub-arrays using temp array
# b = copy of a[1..m]
# i = 1, j = m+1, k = 1
# while i <= m and j <= n,
#     a[k++] = (a[j] < b[i]) ? a[j++] : b[i++]
#     → invariant: a[1..k] in final position
# while i <= m,
#     a[k++] = b[i++]
#     → invariant: a[1..k] in final position

def mergeSort(alist):
    print("Splitting ",alist)
    if len(alist)>1:
        mid = len(alist)//2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k]=lefthalf[i]
                i=i+1
            else:
                alist[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            alist[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            alist[k]=righthalf[j]
            j=j+1
            k=k+1
    print("Merging ",alist)

alist = [3, 4, 5, 7, 2, 1, 6]
mergeSort(alist)
print(alist)
