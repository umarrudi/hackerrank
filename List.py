

class List:
  def __init__(self, content=None, next=None):
    self.content = content
    self.next = next
  def __str__(self):
    return str(self.content)

l1 = List("Umar")
l2 = List("Qonita")
l3 = List("Nafila")
l4 = List("Fahima")
l1.next = l2
l2.next = l3
l3.next = l4
#l4.next = l1
print(type(l1))
#print(l1)
lp = l1
while lp is not None:
  print(lp)
  lp = lp.next