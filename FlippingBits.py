n = 2147483647
n = 1 # 0001
n = 9 # 1001

import ctypes

def flippingBits(n):
  # Logical operator , teori pada catatan halaman 31
  print( format( n, '032b'))
  a = ctypes.c_uint32( ~n ).value
  print( format(a, '032b') )

flippingBits( n )